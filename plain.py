#!/usr/bin/python3
import os,time,enum,re,shutil,sys
from distutils.dir_util import copy_tree
from PIL import Image,ImageEnhance

#globals
PATH_CONFIG = "_config"

TMPL_TAG_HEADER = "HEADER"
TMPL_TAG_TMP = "TMP"
TMPL_TAG_LOC = "LOC"
TMPL_TAG_SHORTCUTS = "SHORTCUTS"
TMPL_TAG_BLOCK = "BLOCK"
TMPL_TAG_ARTICLE = "ARTICLE"
TMPL_TAG_TEXT = "TEXT"
TMPL_TAG_GLIST = "GLIST"
TMPL_TAG_GDETAIL = "GDETAIL"
TMPL_TAG_FOOTER = "FOOTER"

TMPL_MARKER_PAGETITLE = "PAGETITLE"
TMPL_MARKER_PAGEDESC = "PAGEDESC"
TMPL_MARKER_CSS = "CSS"
TMPL_MARKER_CONTENT = "CONTENT"
TMPL_MARKER_ID = "ID"
TMPL_MARKER_DATE = "DATE"
TMPL_MARKER_PREVIOUS = "PREVIOUS"
TMPL_MARKER_NEXT = "NEXT"
TMPL_MARKER_GDETAIL_TITLE = "GDETAIL_TITLE"
TMPL_MARKER_GDETAIL_IMAGE = "GDETAIL_IMAGE"
TMPL_MARKER_GDETAIL_INFO = "GDETAIL_INFO"
TMPL_MARKER_GDETAIL_LIST = "GDETAIL_LIST"

TMPL_MARKER_LOC = "LOC"
TMPL_MARKER_SHORTCUTS = "SHORTCUTS"

###########
# commons #
###########

class PTYPE(enum.Enum):
	UNDEFINED = 0
	PAGE = 1
	GALLERY = 2

class BTYPE(enum.Enum):
	UNDEFINED = 0
	BLOCK = 1
	ARTICLE = 2
	TEXT = 3

# helper classes does not have dump methods

class value_parser:
	
	def __init__(self):
		self.tmp = None
	
	def has_key(self, txt, key ):
		self.tmp = None
		if txt.strip().startswith(key):
			return True
		return False
	
	def load_value(self, txt, key ):
		self.tmp = None
		if txt.startswith(key):
			v = txt[len(key):].strip()
			if len(v) > 0:
				self.tmp = v
				return True
			return False
	
	def load_path( self, txt, key ):
		if self.load_value( txt, key ):
			if self.tmp != None and os.path.isdir(self.tmp):
				return True
			else:
				print( "ERROR, invalid path", key, self.tmp )
		return False
	
	def load_int( self, txt, key ):
		if self.load_value( txt, key ):
			try:
				i = int(self.tmp)
			except:
				print( "ERROR, invalid integer", key, self.tmp )
			if self.tmp != None:
				self.tmp = int(self.tmp)
				return True
			else:
				print( "ERROR, invalid integer", key, self.tmp )
		return False

class path_cleaner:
	
	@staticmethod
	def path( text ):
		t = text.lower().strip()
		t = t.replace(' ','_')
		t = t.replace("'",'')
		t = t.replace('"','')
		t = t.replace("#",'')
		t = t.replace("/",'|')
		t = t.replace("\\",'|')
		return t
	
	@staticmethod
	def filename( text ):
		filename = os.path.basename( text )
		return text.replace(filename, path_cleaner.path(os.path.basename(filename)))

class html_parser:
	
	def __init__(self):
		self.tmp = None
	
	def replace_marker( self, content, marker, text ):
		return content.replace( '#'+marker+'#', text )
	
	def find_marker( self, content, marker ):
		return content.find( '#'+marker+'#' )
	
	def clear_tmp( self, content ):
		self.tmp = None
		self.extract_tag( content, TMPL_TAG_TMP, True )
		while self.tmp != None:
			content = content.replace( self.tmp, '' )
			self.extract_tag( content, TMPL_TAG_TMP, True )
		return content
	
	def extract_tag( self, content, tag, with_tag=False ):
		self.tmp = None
		startat = content.find('<!--#'+tag+'_#-->')
		endat = content.find('<!--#_'+tag+'#-->')
		if startat < 0 or endat < 0 or endat < startat:
			return False
		if with_tag:
			extract = content[startat:endat+len(tag)+10]
		else:
			extract = content[startat+len(tag)+10:endat]
		if tag != TMPL_TAG_TMP:
			extract = self.clear_tmp( extract )
		self.tmp = extract
		return True

class file_utils:
	
	@staticmethod
	def date( path ):
		date = os.path.getmtime(path)
		return time.strftime('%Y.%m.%d %H:%M:%S', time.localtime(date))

#############
# resources #
#############

class image_library:
	
	def __init__(self, config):
		self.config = config
		self.reset()
	
	def reset(self):
		self.images = None
	
	def get(self, relative):
		for k in self.images:
			if k == relative:
				return self.images[k]
		return None
	
	def load(self):
		isc = scanner( self.config )
		self.images = {}
		for root, dirs, files in os.walk( self.config.path.content ):
			for d in dirs:
				isc.image_scan( os.path.join( root, d ) )
				for i in isc.items:
					if self.get( i.relative ) == None:
						self.images[i.relative] = i
		'''
		for k in self.images:
			i = self.images[k]
			i.get_web()
			i.get_thumb()
			print('###',k)
			i.dump_available()
		'''
	
	def dump(self):
		print("\n###### class.image_library dump")
		print( self )
		if self.images == None:
			print( "\t no images" )
			return
		print( "\t-images:", len(self.images) )
		for k in self.images:
			self.images[k].dump()

class text_processor:
	
	def __init__(self, config):
		self.config = config
		self.structures = [ '#h1:','#h2:','#h3:','#p:','#ul:','#table:','#gallery:', '#loc', '#shortcuts' ]
		self.reset()
	
	def reset(self):
		self.relative = None
		self.tmp = []
		self.images = []
	
	def clear_end( self, text, stop = -1 ):
		count = 0
		while ( count < stop or stop == -1 ) and ( text[-1:] == '\n' or text[-1:] == '\r' or text[-1:] == ' ' ):
			text = text[:-1]
			count += 1
		return text
	
	def return_to_br( self, text ):
		text = text.replace( '\n', '<br/>' )
		text = text.replace( '\r', '<br/>' )
		return text
	
	def parse_a( self, text ):
		
		charet = 0
		texlen = len(text)
		newtext = ''
		
		while charet < texlen:
		
			if text[charet:charet+2] == '#a':
				end = text.index('#', charet + 2)
				if end != -1:
					txt = text[ charet+1:end ]
					data = txt.split('*')
					if len( data ) == 3:
						newtext += '<a href="'+data[1]+'">'+data[2]+'</a>'
					elif len( link_data ) == 4:
						newtext += '<a href="'+data[1]+'" title="'+data[2]+'">'+data[3]+'</a>'
				charet = end+1
			
			if charet >= texlen:
				break
			newtext += text[charet]
			charet += 1
			
		return newtext
	
	def parse_i( self, text ):
		
		charet = 0
		texlen = len(text)
		newtext = ''
		
		while charet < texlen:
		
			if text[charet:charet+2] == '#i':
		
				end = text.index('#', charet + 2)
				if end != -1:
		
					txt = text[ charet+1:end ]
					data = txt.split('*')
		
					# image address is at position 1
					# if path starts with http://, it is not a local image
					if not data[1][:4] == "http":
						ipath = path_cleaner.filename( os.path.join( self.relative, data[1] ) )
						i = self.config.image_library.get( ipath )
						if i == None:
							print( "class.text_processor ERROR, no image found at", data[1] )
							newtext += '<span class="error">MISSING IMAGE:' + data[1] + '</span>'
						else:
							rel = i.get_web()
							if rel != None:
								rel = rel.replace(self.relative,'')
								if len( data ) == 2:
									newtext += '<img src="'+rel+'"/>'
								elif len( link_data ) == 3:
									newtext += '<a href="'+data[2]+'"><img src="'+rel+'"/></a>'
								if not rel in self.images:
									self.images.append( rel )
							else:
								print( "class.text_processor ERROR, no image found at", rel )
								newtext += '<span class="error">MISSING IMAGE:' + rel + '</span>'
					else:
						if len( link_data ) == 2:
							newtext += '<img src="'+data[1]+'"/>'
						elif len( link_data ) == 3:
							newtext += '<a href="'+data[2]+'"><img src="'+data[1]+'"/></a>'
						
				charet = end+1
			
			if charet >= texlen:
				break
			newtext += text[charet]
			charet += 1
		
		self.images = sorted(self.images)
		return newtext
	
	def parse_g( self, text ):
		
		text = text.strip()
		gpath = os.path.join( self.config.path.content, self.relative, text )
		if not os.path.isdir( gpath ):
			print( "class.text_processor ERROR, no gallery found at", gpath )
			return '<span class="error">MISSING GALLERY:' + gpath + '</span>'
		
		# retrieve all images form folder and generate thumbs
		out = ''
		iscan = scanner( self.config )
		iscan.image_scan( gpath, False )
		for i in iscan.items:
			rel = path_cleaner.filename( i.replace(self.config.path.content,'') )
			i = self.config.image_library.get( rel )
			if i != None:
				thumb = i.get_thumb()
				out += '<a href="'+i.relative.replace(self.relative,'')+'.html" title="see image '+i.relative+'">'
				out += '<img src="'+thumb.replace(self.relative,'')+'">'
				out += '</a>'
				if not thumb in self.images:
					self.images.append( thumb )
				if not i.relative in self.images:
					self.images.append( i.relative )
		
		return out
	
	def parse_ul( self, text ):
		lines = text.split('\n')
		out = '<ul>'
		for l in lines:
			if len(l.strip()) > 0:
				out += '<li>'+self.parse_inner(l)+'</li>'
		out += '</ul>'
		return out
		
	def parse_table( self, text ):
		rows = text.split('\n')
		out = '<table>'
		for row in rows:
			if len(row.strip()) > 0:
				out += '<tr>'
				cols = row.split('||')
				for col in cols:
					# no check on empty cells
					out += '<td>'+self.parse_inner(col)+'</td>'
				out += '</tr>'
		out += '</table>'
		return out
			
	def parse_inner( self, text ):
		text = text.replace( '#-#', '<hr/>' )
		text = text.replace( '#c_#', '<pre>' )
		text = text.replace( '#_c#', '</pre>' )
		text = self.parse_a( text )
		text = self.parse_i( text )
		return text
	
	def parse_text( self, content ):
		tmp_path = '__tmp'
		f = open( tmp_path, 'w')
		f.write( content )
		f.close()
		self.parse( tmp_path )
		os.remove( tmp_path )
	
	def parse( self, path ):
		self.reset()
		# getting relative path of file
		f = os.path.basename(path)
		self.relative = path.replace( f, '' ).replace( self.config.path.content, '' )
		name = path_cleaner.filename(f)
		# splitting text into structures
		text = open(path).read()
		charet = 0
		texlen = len(text)
		curr = 0
		self.tmp.append( {'tag':'#p','id':curr,'text':'','html':''} )
		while charet < texlen:
			if text[charet] == '#': # marker found
				for s in self.structures:
					if text[charet:charet+len(s)] == s:
						charet += len(s)
						curr = len( self.tmp )
						self.tmp.append( {'tag':s, 'id':curr,'text':'','html':''} )
						break
			if curr != -1:
				self.tmp[curr]['text'] += text[charet]
			charet += 1
		# cleanup
		for i in self.tmp:
			tag = i['tag']
			if tag == '#h1:' or tag == '#h2:' or tag == '#h3:':
				i['text'] = self.clear_end( i['text'] )
				if i['tag'] == '#h1:':
					i['html'] = '<h1>'+i['text']+'</h1>'
				if i['tag'] == '#h2:':
					i['html'] = '<h2>'+i['text']+'</h2>'
				if i['tag'] == '#h3:':
					i['html'] = '<h3>'+i['text']+'</h3>'
			elif tag == '#gallery:':
				i['html'] = self.parse_g(i['text'])
			elif tag == '#ul:':
				i['html'] = self.parse_ul(i['text'])
			elif tag == '#table:':
				i['html'] = self.parse_table(i['text'])
			elif tag == '#p:':
				i['text'] = self.clear_end( i['text'], 1 )
				i['html'] = self.return_to_br( i['text'] )
				i['html'] = self.parse_inner( i['html'] )
				i['html'] = '<p>'+i['html']+'</p>'
			i['html'] = '<a name="'+name+str(i['id'])+'"></a>'+i['html']

#################
# configuration #
#################

class config_path:

	def __init__(self):
		
		self.content = None
		self.web = None
		self.tmpl = None
		self.about = None

	def dump(self):
		print("\n###### class.config_path dump")
		print( self )
		print( "\t-content:", self.content)
		print( "\t-web:", self.web)
		print( "\t-tmpl:", self.tmpl)
		print( "\t-about:", self.about)
		
class config_ftp:

	def __init__(self):
		self.address = None
		self.folder = ""
		self.user = None
		self.password = None
		self.valid = False
	
	def validate( self ):
		self.valid = False
		if self.address == None:
			return
		if self.user == None:
			return
		self.valid = True
	
	def dump(self):
		print("\n###### class.config_ftp dump")
		print( self )
		print( "\t-address:", self.address)
		print( "\t-folder:", self.folder)
		print( "\t-user:", self.user)
		print( "\t-password:", self.password)
		print( "\t-valid:", self.valid)

class config_git:

	def __init__(self):
		self.address = None
		self.user = None
		self.password = None
		self.valid = False
	
	def validate( self ):
		self.valid = False
		if self.address == None:
			return
		if self.user == None:
			return
		self.valid = True
	
	def dump(self):
		print("\n###### class.config_git dump")
		print( self )
		print( "\t-address:", self.address)
		print( "\t-user:", self.user)
		print( "\t-password:", self.password)
		print( "\t-valid:", self.valid)

class config_image:
	
	def __init__(self):
		self.thumb = 100
		self.illu = 30
		self.maxw = 800
		self.maxh = 800
		self.exthd = ['.bmp','.tiff']
		self.ext = ['.svg','.jpg','.jpeg','.gif','.png']

	def dump(self):
		print("\n###### class.config_image dump")
		print( self )
		print( "\t-thumb:", self.thumb)
		print( "\t-illu:", self.illu)
		print( "\t-maxw:", self.maxw)
		print( "\t-maxh:", self.maxh)
		print( "\t-imghd:", self.exthd)
		print( "\t-img:", self.ext)

class config_file:
	
	def __init__(self):
		self.page = '_p.page'
		self.gallery = '_p.gallery'
		self.block = '_b.'
		self.article_suffix = '.article'
		self.text_suffix = '.text'
		
	def dump(self):
		print("\n###### class.config_file dump")
		print( self )
		print( "\t-page:", self.page)
		print( "\t-gallery:", self.gallery)
		print( "\t-block:", self.block)
		print( "\t-article_suffix:", self.article_suffix)
		print( "\t-text_suffix:", self.text_suffix)

class config:
	
	def __init__(self):
		self.reset()
		self.load()
		print( "configuration initialised" )
	
	def reset(self):
		self.valid = False
		self.path = config_path()
		self.file = config_file()
		self.image = config_image()
		self.ftp = config_ftp()
		self.git = config_git()
		self.log = True
		self.autogit = False
		# singletons
		self.image_library = None
		self.text_processor = None
		# value parser
		self.vp = value_parser()
	
	def load_value(self, txt, key ):
		if txt.startswith(key):
			v = txt[len(key):].strip()
			if len(v) > 0:
				self.vp.tmp = v
				return True
			return False
	
	def load_path( self, txt, key ):
		self.vp.tmp = None
		if self.load_value( txt, key ):
			if self.vp.tmp != None and os.path.isdir(self.vp.tmp):
				return True
			else:
				print( "ERROR, invalid path", key, self.vp.tmp )
		return False
	
	def load_int( self, txt, key ):
		self.vp.tmp = None
		if self.load_value( txt, key ):
			try:
				i = int(self.vp.tmp)
			except:
				print( "ERROR, invalid integer", key, self.vp.tmp )
			if self.vp.tmp != None:
				self.vp.tmp = int(self.vp.tmp)
				return True
			else:
				print( "ERROR, invalid integer", key, self.vp.tmp )
		return False
	
	def load( self ):
		
		if not os.path.isfile(PATH_CONFIG):
			print( "ERROR, no configuration found!, current path:", PATH_CONFIG )
			return
		f = open( PATH_CONFIG, 'r' )
		
		for line in f.readlines():
		
			# path
			if self.vp.load_path( line,'#content:' ): 
				self.path.content = self.vp.tmp
			elif self.vp.load_path( line,'#web:' ): 
				self.path.web = self.vp.tmp
			elif self.vp.load_path( line,'#tmpl:' ): 
				self.path.tmpl = self.vp.tmp
			elif self.vp.load_int( line,'#about:' ): 
				self.path.about = self.vp.tmp
		
			# image
			elif self.vp.load_int( line,'#thumbs:' ): 
				self.image.thumbs = self.vp.tmp
			elif self.vp.load_int( line,'#illus:' ): 
				self.image.illus = self.vp.tmp
			elif self.vp.load_int( line,'#maxw:' ): 
				self.image.maxw = self.vp.tmp
			elif self.vp.load_int( line,'#maxh:' ): 
				self.image.maxh = self.vp.tmp
		
			# ftp
			elif self.vp.load_value( line,'#ftp.address:' ): 
				self.ftp.address = self.vp.tmp
			elif self.vp.load_value( line,'#ftp.folder:' ): 
				self.ftp.folder = self.vp.tmp
			elif self.vp.load_value( line,'#ftp.user:' ): 
				self.ftp.user = self.vp.tmp
			elif self.vp.load_value( line,'#ftp.psswrd:' ): 
				self.ftp.psswrd = self.vp.tmp
		
			# git
			elif self.vp.load_value( line,'#git.address:' ): 
				self.git.address = self.vp.tmp
			elif self.vp.load_value( line,'#git.user:' ): 
				self.git.user = self.vp.tmp
			elif self.vp.load_value( line,'#git.psswrd:' ): 
				self.git.psswrd = self.vp.tmp
		
			# behaviour
			elif line.startswith('#nolog'):
				self.log = False
			elif line.startswith('#autogit'):
				self.autogit = True
		
		# singletons
		self.image_library = image_library( self )
		self.image_library.load()
		self.text_processor = text_processor( self )
		
		self.validate()

	def validate( self ):
		self.ftp.validate()
		self.git.validate()
		if self.path.content == None:
			self.valid = False
			return
		if self.path.web == None:
			self.valid = False
			return
		if self.path.tmpl == None:
			self.valid = False
			return
		self.valid = True
	
	def dump(self):
		print("\n###### class.config dump")
		print( self )
		print( "\t-valid:", self.valid )
		print( "\t-log:", self.log )
		print( "\t-autogit:", self.autogit )
		self.path.dump()
		self.file.dump()
		self.image.dump()
		self.ftp.dump()
		self.git.dump()
		if self.image_library == None:
			print( "\t-image library:", self.image_library )
		else:
			self.image_library.dump()
		print( "\t-text processor:", self.text_processor )

###################
# pages & content #
###################

class page:
	
	def __init__(self, config):
		self.config = config
		self.reset()
	
	def reset(self):
		# system
		self.name = "index"
		self.path = None
		self.folder = None
		self.relative = None
		self.web_path = None
		self.text_path = None
		self.pdf_path = None
		self.zip_path = None
		self.type = PTYPE.UNDEFINED
		self.date = None
		# content
		self.content = None
		self.images = None
		self.title = None
		self.title_clean = None
		self.desc = None
		self.nofoot = False
		self.css = None
		self.shortcuts = None
		# for lists and galleries
		self.nocontent = False
		# for galleries
		self.maxw = self.config.image.maxw
		self.maxh = self.config.image.maxh
		self.valid = False
		# value parser
		self.vp = value_parser()
	
	def generate_shortcuts( self ):
		for c in self.content:
			for e in c.elements:
				if e['tag'] == '#h2:' or e['tag'] == '#h3:':
					if self.shortcuts == None:
						self.shortcuts = []
					self.shortcuts.append( { 
						'href': '#'+c.name+str(e['id']),
						'title': path_cleaner.path(e['text']),
						'text': e['text']
					} )
	
	def load( self, path ):
		
		if not os.path.isfile(path):
			print( "ERROR, no file found!, current path:", path )
			return
		
		f = os.path.basename(path)
		if f == self.config.file.page:
			self.type = PTYPE.PAGE
		elif f == self.config.file.gallery:
			self.type = PTYPE.GALLERY
		else:
			print( "ERROR, this page is not valid:", path )
			return
		
		self.path = path
		self.folder = path[:-len(f)]
		self.date = file_utils.date( path )
		
		f = open( path, 'r' )
		for line in f.readlines():
			if self.vp.load_value( line,'#name:' ): 
				self.name = self.vp.tmp
			elif self.vp.load_value( line,'#title:' ): 
				self.title = self.vp.tmp
			elif self.vp.load_value( line,'#desc:' ): 
				self.desc = self.vp.tmp
			elif self.vp.has_key( line,'#nofoot' ): 
				self.nofoot = True
			elif self.vp.has_key( line,'#nocontent' ):
				self.nocontent = True
			elif self.vp.load_int( line,'#maxw:' ): 
				self.maxw = self.vp.tmp
			elif self.vp.load_int( line,'#maxh:' ): 
				self.maxh = self.vp.tmp
		
		bscanner = scanner( self.config )
		bscanner.block_scan( self.folder )
		self.content = bscanner.items
		for c in self.content:
			# shortcuts creation
			for e in c.elements:
				if e['tag'] == '#shortcuts':
					self.generate_shortcuts() # generates a list
			# collecting images
			for i in c.images:
				if self.images == None:
					self.images = []
				if not i in self.images:
					self.images.append( i )
		
		if self.type == PTYPE.GALLERY:
			# scan for images
			iscanner = scanner( self.config )
			iscanner.image_scan( self.folder, False )
			self.images = []
			for i in iscanner.items:
				self.images.append( path_cleaner.filename( i.replace(self.config.path.content,'') ) )
		
		self.validate()
		
		if self.valid:
			self.title_clean = path_cleaner.path( self.title )
			self.relative_path = self.folder[ len(self.config.path.content): ]
			self.web_path = os.path.join( self.relative_path, self.name + '.html' )
			self.text_path = os.path.join( self.relative_path, self.name + '.text.html' )
			self.pdf_path = os.path.join( self.relative_path, self.title_clean + '.pdf' )
			self.zip_path = os.path.join( self.relative_path, self.title_clean + '.zip' )
	
	def validate(self):
		self.valid = False
		if self.path == None:
			return
		if self.folder == None:
			return
		if self.title == None:
			return
		self.valid = True
	
	def dump(self):
		print("\n###### class.page dump")
		print( self )
		print( "\t-config:", self.config)
		print( "\t-name:", self.name)
		print( "\t-path:", self.path)
		print( "\t-date:", self.date)
		print( "\t-folder:", self.folder)
		print( "\t-relative_path:", self.relative_path)
		print( "\t-web_path:", self.web_path)
		print( "\t-text_path:", self.text_path)
		print( "\t-pdf_path:", self.pdf_path)
		print( "\t-zip_path:", self.zip_path)
		print( "\t-type:", self.type)
		print( "\t-valid:", self.valid)
		print( "\t-title:", self.title)
		print( "\t-desc:", self.desc)
		print( "\t-nofoot:", self.nofoot)	
		print( "\t-nocontent:", self.nocontent)
		print( "\t-maxw:", self.maxw)
		print( "\t-maxh:", self.maxh)
		print( "\t-css:", self.css)
		if self.content == None:
			print( "\t-content:", self.content)
		else:
			print( "\t-content:", len(self.content))
			for b in self.content:
				b.dump()
		if self.images == None:
			print( "\t-images:", self.images)
		else:
			print( "\t-images:", len(self.images))
			for i in self.images:
				print( "\t\t", i )

class block:
	
	def __init__(self, config):
		self.config = config
		self.reset()
	
	def reset(self):
		self.type = BTYPE.UNDEFINED
		self.path = None
		self.date = None
		self.name = None
		self.content = None
		self.elements = None
		self.images = []
		self.valid = False
	
	def empty(self):
		self.reset()
		self.type = BTYPE.BLOCK
		self.content = ''
		self.valid = True
	
	def load(self, path):
		
		self.reset()
		
		if not os.path.isfile(path):
			print( "class.block ERROR, no file found!, current path:", path )
			return
			
		self.path = path
		self.date = file_utils.date( path )
		self.name = path_cleaner.filename(os.path.basename(path))
		self.type = BTYPE.BLOCK
		if self.path[-len(self.config.file.article_suffix):] == self.config.file.article_suffix:
			self.type = BTYPE.ARTICLE
		elif self.path[-len(self.config.file.text_suffix):] == self.config.file.text_suffix:
			self.type = BTYPE.TEXT
		
		self.content = open(self.path).read()
		
		self.parse()
		
		self.valid = True
	
	def parse(self):
		
		tp = self.config.text_processor
		if self.path != None:
			tp.parse( self.path )
		elif self.content != None:
			tp.parse_text( self.content )
		else:
			return
		self.elements = tp.tmp
		for i in tp.images:
			self.images.append(i)
	
	def dump(self):
		print("\n###### class.block dump")
		print( self )
		print( "\t-path:", self.path)
		print( "\t-date:", self.date)
		print( "\t-valid:", self.valid)
		print( "\t-type:", self.type)
		print( "\t-content:", '\n'+self.content)
		print( "\t-elements:", len(self.elements))
		for e in self.elements:
			print( "\t", e['tag'] )
			print( "\t\t", e['html'] )
		print( "\t-images:", len(self.images))
		for i in self.images:
			print( "\t\t", i )

class image:
	
	def __init__(self, config):
		self.config = config
		self.reset()
	
	def reset(self):
		self.original = None
		self.name = None
		self.date = None
		self.folder = None
		self.web = None
		self.relative = None
		self.ext = None
		self.size = None
		self.valid = False
		self.available = []
	
	def load(self, folder, path):
		
		self.valid = False
		if not os.path.isfile( path ):
			self.path = None
			print( "ERROR, cannot load image", path )
			return
		
		self.original = path
		self.date = file_utils.date( path )
		self.folder = folder
		
		cleanpath = path_cleaner.filename( self.original )
				
		self.web = cleanpath.replace( self.config.path.content, self.config.path.web )
		self.relative = self.web.replace( self.config.path.web, '' )
		
		lc = path.lower()
		for e in self.config.image.ext:
			if lc[-len(e):] == e:
				self.ext = e
				break
		self.name = path_cleaner.filename( os.path.basename( path ) ).replace( self.ext, '' )
		
		# verification of image
		if self.ext != '.svg':
			im = Image.open( self.original )
			self.size = im.size
			im.close()
				
		# storing path
		self.add_path( path )
		if os.path.isfile( self.web ): 
			self.add_path( self.web )
		
		self.valid = True
	
	def add_path(self, path):
		for p in self.available:
			if p == path:
				return False
		self.available.append( path )
		return True
	
	def thumb_suffix(self, size):
		return ".thumb_"+str(size[0])+"x"+str(size[1])+self.ext
	
	def illu_suffix(self, size):
		return ".illu_"+str(size[0])+"x"+str(size[1])+self.ext
	
	def exact_thumb( self, width, height, destination ):
		src = Image.open(self.original)
		src_ratio = src.size[1]*1.0/src.size[0]
		thumb_ratio = height*1.0/width
		crop_width = 0
		crop_height = 0
		crop_offset = [0,0]
		if src_ratio <= thumb_ratio:
			crop_width = int(src.size[1] / thumb_ratio)
			crop_height = src.size[1]
			crop_offset[0] = int((src.size[0]-crop_width)/2)
		else:
			crop_width = src.size[0]
			crop_height = int(src.size[0] * thumb_ratio)
			crop_offset[1] = int((src.size[1]-crop_height)/2)
		thumb = Image.new("RGB", (crop_width, crop_height), "black")
		thumb.paste(src, (-crop_offset[0],-crop_offset[1]))
		thumb = thumb.resize((width,height), Image.LANCZOS)
		thumb.save( destination )
		thumb.close()
		src.close()
	
	def ckech_folder( self ):
		trgt_folder = self.folder.replace( self.config.path.content, self.config.path.web )
		if not os.path.isdir(trgt_folder):
			os.makedirs(trgt_folder)
	
	def get_thumb( self, size = None ):
		if self.ext == '.svg':
			return self.relative
		self.ckech_folder()
		if size == None or not size is Array:
			size = [self.config.image.thumb, self.config.image.thumb]
		suffix = self.thumb_suffix( size )
		p = self.web+suffix
		p_rel = self.relative+suffix
		if not os.path.isfile(p):
			self.exact_thumb( size[0], size[1], p )
		self.add_path( p )
		return p_rel
	
	def get_illu( self, size = None ):
		if self.ext == '.svg':
			return self.relative
		self.ckech_folder()
		if size == None or not size is Array:
			size = [self.config.image.illu, self.config.image.illu]
		suffix = self.illu_suffix( size )
		p = self.web+suffix
		p_rel = self.relative+suffix
		if not os.path.isfile(p):
			self.exact_thumb( size[0], size[1], p )
		self.add_path( p )
		return p_rel
	
	def get_web( self, size = None ):
		cpi = False
		rsi = False
		p = self.web
		p_rel = self.relative
		if size == None or not size is list:
			size = [ self.config.image.maxw, self.config.image.maxh ]
		elif size[0] != self.config.image.maxw or size[1] != self.config.image.maxh:
			suffix = "_"+str(size[0])+'x'+str(size[1])+self.ext
			p += suffix
			p_rel += suffix
		if not os.path.isfile(self.web):
			cpi = True
		if self.config.image.maxw < size[0] or self.config.image.maxh < size[1]:
			cpi = True
			rsi = True
		if cpi:
			self.ckech_folder()
			if self.ext == '.svg':
				shutil.copyfile( self.original, p )
			else:
				im = Image.open(self.original)
				if rsi:
					im.thumbnail(size, Image.LANCZOS)
				im.save(p)
				im.close()
		self.add_path(p)
		return p_rel
	
	def dump_available(self):
		print( "available images for", self.original )
		for p in self.available:
			print('\t',p)
	
	def dump(self):
		print("\n###### class.image dump")
		print( self )
		print( "\t-config:", self.config)
		print( "\t-name:", self.name)
		print( "\t-valid:", self.valid)
		print( "\t-original:", self.original)
		print( "\t-date:", self.date)
		print( "\t-folder:", self.folder)
		print( "\t-web:", self.web)
		print( "\t-relative:", self.relative)
		print( "\t-ext:", self.ext)
		print( "\t-size:", self.size)
		self.dump_available()

############
# template #
############

class tmpl:
	
	def __init__(self, config):
		self.config = config
		self.hp = html_parser()
		self.reset()
	
	def reset(self):
		
		self.path = self.config.path.tmpl
		self.date = file_utils.date( self.path )
		self.valid = False
		self.html = None
		self.css = None
		self.link_css = None
		self.blocks = None
		
		if not os.path.isdir(self.path):
			self.path = None
			print( "ERROR, path does not exists:", path )
			return
		hpath = os.path.join(self.path,'index.html')
		if not os.path.isfile(hpath):
			self.path = None
			print( "ERROR, no 'index.html' found:", path )
			return
		self.html = open( hpath, 'r' ).read()
		cpath = os.path.join(self.path,'main.css')
		if not os.path.isfile(cpath):
			self.path = None
			print( "WARNING, no 'main.css' found:", path )
		else:
			self.css = cpath
					
		self.load_tags()
		
		self.separator_locator = None
		self.separator_shortcuts = None
		if TMPL_TAG_LOC in self.blocks:
			res = self.extract_separator( TMPL_MARKER_LOC, self.blocks[TMPL_TAG_LOC] )
			self.separator_locator = res[0]
			self.blocks[TMPL_TAG_LOC] = res[1]
		if TMPL_TAG_SHORTCUTS in self.blocks:
			res = self.extract_separator( TMPL_MARKER_SHORTCUTS, self.blocks[TMPL_TAG_SHORTCUTS] )
			self.separator_shortcuts = res[0]
			self.blocks[TMPL_TAG_SHORTCUTS] = res[1]
		
		self.validate()
	
	def push_tag( self, tag, default = None ):
		if self.hp.extract_tag( self.html, tag ):
			if len(self.hp.tmp.strip()) > 0:
				self.blocks[ tag ] = self.hp.tmp
				return
		self.blocks[ tag ] = default
	
	def extract_separator(self, marker, text):
		charet = 0
		texlen = len(text)
		newtext = ''
		separator = None
		while charet < texlen:
			if text[charet] == '#' and text[charet:charet+len(marker)+1] == '#'+marker:
				end = text.index('#', charet+len(marker)+1)
				if end != -1:
					txt = text[ charet+1:end ]
					data = txt.split(':')
					if len(data) >= 2:
						separator = data[1]
				newtext += '#'+TMPL_MARKER_CONTENT+'#'
				charet = end+1
			if charet >= texlen:
				break
			newtext += text[charet]
			charet += 1
		return [separator,newtext]
	
	def load_tags(self):
		
		self.blocks = None
		if self.html == None:
			return
		
		self.blocks = {}
		# searching for blocks
		default_content = '#'+TMPL_MARKER_CONTENT+'#'
		self.push_tag( TMPL_TAG_HEADER )
		self.push_tag( TMPL_TAG_FOOTER )
		self.push_tag( TMPL_TAG_LOC )
		self.push_tag( TMPL_TAG_SHORTCUTS )
		self.push_tag( TMPL_TAG_ARTICLE, default_content )
		self.push_tag( TMPL_TAG_BLOCK, default_content )
		self.push_tag( TMPL_TAG_TEXT, default_content )
		self.push_tag( TMPL_TAG_GLIST )
		self.push_tag( TMPL_TAG_GDETAIL )
	
	def wrap_content(self, tag, content):
		if not tag in self.blocks:
			return content
		return self.hp.replace_marker( self.blocks[tag], TMPL_MARKER_CONTENT, content )
	
	def image_detail(self, title, im, info, gallery):
		
		if not TMPL_TAG_GDETAIL in self.blocks:
			return 'NO TMPL_TAG_GDETAIL in ' + self.path
		
		out = self.blocks[TMPL_TAG_GDETAIL]
		out = self.hp.replace_marker( out, TMPL_MARKER_GDETAIL_TITLE, title )
		out = self.hp.replace_marker( out, TMPL_MARKER_GDETAIL_IMAGE, im )
		out = self.hp.replace_marker( out, TMPL_MARKER_GDETAIL_INFO, info )
		return self.hp.replace_marker( out, TMPL_MARKER_GDETAIL_LIST, gallery )
		
	
	def replace_navigation(self, text, marker, path):
		charet = 0
		texlen = len(text)
		newtext = ''
		separator = None
		while charet < texlen:
			if text[charet] == '#' and text[charet:charet+len(marker)+1] == '#'+marker:
				end = text.index('#', charet+len(marker)+1)
				if end != -1:
					txt = text[ charet+1:end ]
					data = txt.split(':')
					if len(data) >= 2 and path != None:
						newtext += '<a href="'+path+'">'+data[1]+'</a>'
				charet = end+1
			if charet >= texlen:
				break
			newtext += text[charet]
			charet += 1
		return newtext
	
	def replace_marker(self, content, marker, text):
		if marker == TMPL_MARKER_NEXT or marker == TMPL_MARKER_PREVIOUS:
			return self.replace_navigation( content, marker, text )
		return self.hp.replace_marker( content, marker, text )
	
	def validate(self):
		
		self.valid = False
		if self.path == None:
			return
		if self.html == None:
			return
		if self.blocks == None:
			return
		else:
			
			if not TMPL_TAG_HEADER in self.blocks:
				print( "ERROR, template must have a '"+ TMPL_TAG_HEADER +"' tag" )
				return
			if not TMPL_TAG_FOOTER in self.blocks:
				print( "ERROR, template must have a '"+ TMPL_TAG_FOOTER +"' tag" )
				return
			
			if not TMPL_TAG_BLOCK in self.blocks:
				print( "ERROR, template must have a '"+ TMPL_TAG_BLOCK +"' tag" )
				return
			elif self.hp.find_marker( self.blocks[TMPL_TAG_BLOCK], TMPL_MARKER_CONTENT ) < 0:
				print( "ERROR, '"+ TMPL_TAG_BLOCK +"' tag must have a '"+TMPL_MARKER_CONTENT+"' marker inside them" )
				return
			
			if TMPL_TAG_ARTICLE in self.blocks and self.hp.find_marker( self.blocks[TMPL_TAG_ARTICLE], TMPL_MARKER_CONTENT ) < 0:
				print( "ERROR, '"+ TMPL_TAG_ARTICLE +"' tag must have a '"+TMPL_MARKER_CONTENT+"' marker inside them" )
				return
			if TMPL_TAG_GLIST in self.blocks and self.hp.find_marker( self.blocks[TMPL_TAG_GLIST], TMPL_MARKER_CONTENT ) < 0:
				print( "ERROR, '"+ TMPL_TAG_ARTICLE +"' tag must have a '"+TMPL_MARKER_CONTENT+"' marker inside them" )
				return
			if TMPL_TAG_GDETAIL in self.blocks:
				if self.hp.find_marker( self.blocks[TMPL_TAG_GDETAIL], TMPL_MARKER_GDETAIL_IMAGE ) < 0:
					print( "ERROR, '"+ TMPL_TAG_GDETAIL +"' tag must have a '"+TMPL_MARKER_GDETAIL_IMAGE+"' marker inside them" )
					return
			elif TMPL_TAG_GDETAIL in self.blocks:
				if self.hp.find_marker( self.blocks[TMPL_TAG_GDETAIL], TMPL_MARKER_GDETAIL_INFO ) < 0:
					print( "ERROR, '"+ TMPL_TAG_GDETAIL +"' tag must have a '"+TMPL_MARKER_GDETAIL_INFO+"' marker inside them" )
					return
			elif TMPL_TAG_GDETAIL in self.blocks:
				if self.hp.find_marker( self.blocks[TMPL_TAG_GDETAIL], TMPL_MARKER_GDETAIL_LIST ) < 0:
					print( "ERROR, '"+ TMPL_TAG_GDETAIL +"' tag must have a '"+TMPL_MARKER_GDETAIL_LIST+"' marker inside them" )
					return
		
		# copying all folder to web:
		copy_tree( self.path, os.path.join( self.config.path.web, 'css' ))
		self.link_css = os.path.join( 'css', 'main.css' )
		
		self.valid = True			
	
	def dump(self):
		print("\n###### class.tmpl dump")
		print(self)
		print('\t-config:', self.config)
		print('\t-path:', self.path)
		print('\t-date:', self.date)
		print('\t-valid:', self.valid)
		print('\t-css:', self.css)
		print('\t-link_css:', self.link_css)
		print('\t-html (file length):', len(self.html))
		if self.blocks == None:
			print('\t-blocks:', self.blocks)
		else:
			print('\t-blocks:', len(self.blocks))
			for k in self.blocks:
				out = self.blocks[k].strip().replace('  ',' ').replace('\t','').replace('\n','').replace('\r','')
				print('\t\t-', k)
				print('\t\t', out[:500] + '...')

###########
# scanner #
###########
	
class scanner:
	
	def __init__(self, config):
		self.config = config
		self.reset()
	
	def reset(self):
		self.folder = None
		self.items = []
	
	def page_scan( self, path ):
		files = os.listdir( path )
		ptype = PTYPE.UNDEFINED
		file = None
		for f in files:
			if f == self.config.file.page:
				ptype = PTYPE.PAGE
				file = os.path.join( path, f )
			elif ptype != PTYPE.PAGE and f == self.config.file.gallery:
				ptype = PTYPE.GALLERY
				file = os.path.join( path, f )
		return file
	
	def block_scan(self, path):
		self.folder = path
		files = os.listdir( path )
		files = sorted(files)
		for f in files:
			if f[:len(self.config.file.block)] == self.config.file.block:
				b = block( self.config )
				b.load( os.path.join( self.folder, f ) )
				if b.valid:
					self.items.append( b )
	
	def image_scan(self, path, create_objects = True):
		self.folder = path
		files = os.listdir( path )
		for f in files:
			lc = f.lower()
			for e in self.config.image.ext:
				if lc[-len(e):] == e:
					keepon = False
					i = None
					if create_objects:
						i = image( self.config )
						i.load( path, os.path.join( path, f ) )
						if i.valid:
							self.items.append( i )
					else:
						i = os.path.join( path, f ).replace( self.config.path.content, '' )
						self.items.append( i )
					break
	
	def content_scan(self, path):
		self.folder = path
		for root, dirs, files in os.walk( self.folder ):
			for dir in dirs:
				fullp = os.path.join( root, dir )
				file = self.page_scan( fullp )
				if file != None:
					p = page( self.config )
					p.load( file )
					if p.valid:
						self.items.append( p )
	
	def dump(self):
		print("\n###### class.scanner dump")
		print( self )
		print( "\t-config:", self.config )
		print( "\t-folder:", self.folder )
		print( "\t-items:", len( self.items ) )
		for p in self.items:
			p.dump()

###############
# html output #
###############

class html:
	
	def __init__(self, config):
		self.config = config
		self.template = None
		self.hp = html_parser()
	
	def generate_header( self, page ):
		
		header = template.blocks[TMPL_TAG_HEADER]
		header = self.hp.replace_marker( header, TMPL_MARKER_PAGETITLE, page.title_clean )
		header = self.hp.replace_marker( header, TMPL_MARKER_PAGEDESC, page.desc )
		# counting depth:
		fhierarchy = page.folder.split('/')
		print(fhierarchy)
		depth = 0
		for fh in fhierarchy:
			if len(fh) > 0:
				depth += 1
		if depth > 0:
			depth -= 1
		# css path
		cssp = ''
		for i in range(0,depth):
			cssp += '../'
		cssp = '<link rel="stylesheet" href="'+cssp+template.link_css+'" type="text/css">'
		header = self.hp.replace_marker( header, TMPL_MARKER_CSS, cssp )
		return header
	
	def wrap_block( self, block, html ):
		
		if block.type == BTYPE.UNDEFINED:
			return html
		
		if block.type == BTYPE.ARTICLE:
			html = template.wrap_content( TMPL_TAG_ARTICLE, html )
		elif block.type == BTYPE.BLOCK:
			html = template.wrap_content( TMPL_TAG_BLOCK, html )
		elif block.type == BTYPE.TEXT:
			html = template.wrap_content( TMPL_TAG_TEXT, html )
		
		return html
	
	def generate_content( self, template, page ):
		
		html = ''
		
		prevs = []
		nexts = []
		num = len(page.content)
		for i in range(0,num):
			if i == 0:
				prevs.append( None )
			else:
				prevs.append( '#'+page.content[i-1].name )
			if i < num -1:
				nexts.append( '#'+page.content[i+1].name )
			else:
				nexts.append( None )
		
		index = 0
		for b in page.content:
			
			sub_html = ''
			
			if b.type == BTYPE.UNDEFINED:
				index += 1
				continue
			elif b.type == BTYPE.TEXT:
				sub_html = b.content
			else:
				
				# parsing elements
				for e in b.elements:
					
					if e['tag'] == '#gallery:':
						sub_html += template.wrap_content( TMPL_TAG_GLIST, e['html'] )
					
					elif e['tag'] == '#shortcuts':
						if page.shortcuts != None:
							tmp_html = ''
							sep = template.separator_shortcuts
							if sep == None:
								sep = ' '
							firstsc = True
							for sc in page.shortcuts:
								if len(tmp_html) > 0:
									tmp_html += sep
								tmp_html += '<a href="'+ sc['href']+'" '
								tmp_html += 'title="'+ sc['title']+'">'
								tmp_html += sc['title']+'</a>'
							sub_html += template.wrap_content( TMPL_TAG_SHORTCUTS, tmp_html )
					
					else:
						
						sub_html += e['html']
			
			sub_html = self.wrap_block( b, sub_html )
			sub_html = template.replace_marker( sub_html, TMPL_MARKER_ID, b.name )
			sub_html = template.replace_marker( sub_html, TMPL_MARKER_DATE, b.date )
			sub_html = template.replace_marker( sub_html, TMPL_MARKER_PREVIOUS, prevs[index] )
			sub_html = template.replace_marker( sub_html, TMPL_MARKER_NEXT, nexts[index] )
			html += sub_html
			
			index += 1
				
		return html
	
	def generate_gallery( self, template, page, image ):
		
		page.dump
		
		wpath = os.path.join( self.config.path.web, image + '.html' )
		im = self.config.image_library.get( image )
		if im == None:
			print( "class.html ERROR, no image at", image )
			return
		# making sure web version is there
		ipath = im.get_web( [page.maxw,page.maxh] )
		ititle = page.title_clean + ' : '+ im.name
		
		f = open( wpath, 'w' )
		
		tc_bu = page.title_clean
		page.title_clean = ititle
		f.write( self.generate_header( page ) )
		page.title_clean = tc_bu
		
		top_block = block( page.config )
		top_block.empty()
		top_block.content += '#h1:'+ititle
		top_block.content += '#p:'+page.desc
		top_block.parse()
		#top_block.dump()
		html = ''
		for e in top_block.elements:
			html += e['html']
		f.write( template.wrap_content( TMPL_TAG_BLOCK, html ) )
		
		# #GDETAIL_IMAGE#
		# #GDETAIL_INFO#
		# #GDETAIL_LIST#
		image_html = '<img src="'+os.path.basename(ipath)+'"/>'
		info_html = im.name+im.ext + ', '+ im.date
		list_html = 'LIST / LIST / LIST / LIST / LIST / LIST'
		
		f.write( template.image_detail( ititle, image_html, info_html, list_html ) )
		f.write( self.generate_content( template, page ) )
		
		if not page.nofoot:
			f.write( template.blocks[TMPL_TAG_FOOTER] )
		f.close()
	
	def generate( self, template, pages ):
		
		if not template.valid:
			print( "class.html ERROR, template is NOT valid!" )
			return
		
		self.template = template
		for p in pages:
			trgt_folder = p.folder.replace( self.config.path.content, self.config.path.web )
			# build output folder
			if not os.path.isdir(trgt_folder):
				os.makedirs( trgt_folder )
			# output file
			if p.type == PTYPE.PAGE:
				f = open( os.path.join( self.config.path.web, p.web_path ), 'w' )
				## HEADER
				f.write( self.generate_header( p ) )
				## CONTENT
				f.write( self.generate_content( template, p ) )
				## FOOTER
				if not p.nofoot:
					f.write( template.blocks[TMPL_TAG_FOOTER] )
				f.close()
			elif p.type == PTYPE.GALLERY:
				# generation of a page for each image containing
				# a list of all images
				for i in p.images:
					self.generate_gallery( template, p, i )
			print( p.web_path )

conf = config()
#conf.dump()
#conf.image_library.dump()

template = tmpl( conf )
#template.dump()

cscanner = scanner( conf )
cscanner.content_scan( conf.path.content )
#cscanner.dump()

html_site = html( conf )
html_site.generate( template, cscanner.items )
